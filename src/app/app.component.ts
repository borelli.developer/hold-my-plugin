import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Fotocamera', url: '/camera', icon: 'camera' },
    { title: 'QR Code Scanner', url: '/qr-scanner', icon: 'scan' },
    { title: 'Bluetooth', url: '/bluetooth', icon: 'bluetooth' },
    { title: 'Eventi di calendario', url: '/calendar-events', icon: 'calendar' },
    { title: 'Geolocalizzazione', url: '/geolocation', icon: 'locate' }
  ];
  constructor() {}
}

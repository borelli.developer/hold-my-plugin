import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.page.html',
  styleUrls: ['./qr-scanner.page.scss'],
})
export class QrScannerPage implements OnInit {
  scannerResult: string;
  constructor(private scanner: BarcodeScanner) { }

  ngOnInit() {

  }

  startScanner() {
    this.scanner.scan().then(scanResult => {
      if(!scanResult.cancelled) {
        this.scannerResult = scanResult.text;
      }
    });
  }

}

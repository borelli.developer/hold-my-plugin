import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalendarEventsPageRoutingModule } from './calendar-events-routing.module';

import { CalendarEventsPage } from './calendar-events.page';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    CalendarEventsPageRoutingModule,
  ],
  declarations: [CalendarEventsPage],
  providers: [Calendar]
})
export class CalendarEventsPageModule {}

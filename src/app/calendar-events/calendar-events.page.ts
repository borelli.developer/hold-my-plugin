import { Component, OnInit } from '@angular/core';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';

@Component({
  selector: 'app-calendar-events',
  templateUrl: './calendar-events.page.html',
  styleUrls: ['./calendar-events.page.scss'],
})
export class CalendarEventsPage implements OnInit {


  newCalendarEvent: CalendarEvent = {};
  constructor(private calendar: Calendar) { }

  ngOnInit() {
  }

  addEventToCalendar() {
    console.log(this.newCalendarEvent)
    this.calendar.createEventInteractively(
      this.newCalendarEvent.title,
      this.newCalendarEvent.location,
      this.newCalendarEvent.notes,
      new Date(this.newCalendarEvent.startDate),
      new Date(this.newCalendarEvent.endDate)
    );

    this.newCalendarEvent = {};
  
  }
}


export interface CalendarEvent {
  title?: string;
  startDate?: Date | string;
  endDate?: Date | string;
  notes?: string;
  location?: string;
}



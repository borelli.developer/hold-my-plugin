import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluetoothPageRoutingModule } from './bluetooth-routing.module';

import { BluetoothPage } from './bluetooth.page';
import { SharedModule } from '../shared/shared.module';
import { BluetoothLE } from '@awesome-cordova-plugins/bluetooth-le/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    BluetoothPageRoutingModule
  ],
  declarations: [BluetoothPage],
  providers: [BluetoothLE]
})
export class BluetoothPageModule {}

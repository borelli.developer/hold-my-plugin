import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BluetoothLE, ScanStatus } from '@awesome-cordova-plugins/bluetooth-le/ngx';
import { delay, takeUntil } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-bluetooth',
  templateUrl: './bluetooth.page.html',
  styleUrls: ['./bluetooth.page.scss'],
})
export class BluetoothPage implements OnInit {

  devices: BLEDevice = {};
  isScanning: boolean = false;
  constructor(public bluetoothle: BluetoothLE, public plt: Platform) {}

  ngOnInit() {
  }

  initializeBluetooth() {
   this.bluetoothle.initialize().subscribe(ble => {
    this.bluetoothle.hasPermission().then((hasPermission) => {
      if(hasPermission) {
        console.log("All required permissions granted for Bluetooth.");
        this.checkLocationEnabled();
      } else {
        this.bluetoothle.requestPermission().then(() => {
          this.checkLocationEnabled();
        })
      }
    })
   
  });
  }


  private checkLocationEnabled() {

    this.bluetoothle.isLocationEnabled().then(isLocationEnabled => {
      if(isLocationEnabled) {
        this.startScanning();
      } else {
        this.bluetoothle.requestLocation().then(() => {
          this.startScanning();
        });
      }
    });
  }


  private startScanning() {
    this.isScanning = true;
    this.bluetoothle.startScan({allowDuplicates: false}).pipe(
      takeUntil(of(null).pipe(delay(5000)))
    ).subscribe({
      next: (scanStatus) => {
        if(scanStatus.address && scanStatus.name) {
          console.log("New BLE Device discovered:", scanStatus);
          this.devices[scanStatus.address] = scanStatus;
        }
      }, 
      complete: () => {
        this.bluetoothle.stopScan();
        this.isScanning = false;
      }
    });
  }

}

export interface BLEDevice {
  [address: string]: ScanStatus;
}